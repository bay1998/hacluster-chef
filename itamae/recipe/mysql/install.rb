packages = node[:mysql][:packages]

define :install_package do
    package params[:name] do
        version node[:mysql][:version] if node[:mysql][:version]
        action :install
    end
end

install_package "mysql" if packages[:mysql]
install_package "mysql-server" if packages[:mysqlserver]

directory "/etc/my.cnf.d" do
    path "/etc/my.cnf.d"
    mode "600"
    owner "mysql"
    group "mysql"
end

execute "old-data-dir" do
    command "rm -rf /var/lib/mysql/"
    only_if "test -e /var/lib/mysql"
end

execute "initialize" do
    command "mysqld --initialize --user=mysql"
end

service "mysqld" do
    action [:start, :enable]
end