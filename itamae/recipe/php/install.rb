packages = node[:php][:packages]

packages.each do |pkg|
    package pkg do
        version node[:php][:version] if node[:php][:version]
        action :install
    end
end