dest_path = "/etc/nginx/nginx.conf"
config = node[:nginx][:config]

service "nginx" do
    action :nothing
end

template dest_path do
    variables({"config" => config})
    source "./template#{dest_path}.erb"
    notifies :reload, "service[nginx]"
end

service "nginx" do
    action [:start, :enable]
end