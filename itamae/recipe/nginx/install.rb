package "nginx" do
    action :install
    version node[:nginx][:version]
end