require 'dotenv'
Dotenv.load
passwords = {"db_password": ENV["DB_1_PASSWORD"],
             "wp_auth_key": ENV["AUTH_KEY"],
             "wp_secure_auth_key": ENV["SECURE_AUTH_KEY"],
             "wp_logged_in_key": ENV["LOGGED_IN_KEY"],
             "wp_nonce_key": ENV["NONCE_KEY"],
             "wp_auth_salt": ENV["AUTH_SALT"],
             "wp_secure_auth_salt": ENV["SECURE_AUTH_SALT"],
             "wp_logged_in_salt": ENV["LOGGED_IN_SALT"],
             "wp_nonce_salt": ENV["NONCE_SALT"]}

wp_path = node[:wordpress][:public]
wp_config = "#{wp_path}/wp-config.php"

execute "permit dir" do
    user "root"
    command "find #{wp_path}/ -type d | xargs chmod 0775"
end

execute "permit file" do
    user "root"
    command "find #{wp_path}/ -type f | xargs chmod 0664"
end

template wp_config do
    owner "nginx"
    group "nginx"
    mode "0400"
    source "template#{wp_config}.erb"
    variables(passwords: passwords)
end
