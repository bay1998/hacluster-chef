version = node[:wordpress][:version]
pub = node[:wordpress][:public]

execute 'wordpress file get' do
    user "root"
    command "cd /tmp && wget https://wordpress.org/wordpress-#{version}.tar.gz -O wordpress.tar.gz"
end

execute 'deployment file' do
    user "root"
    command "tar xvzf /tmp/wordpress.tar.gz"
end

execute 'make dir' do
    user "root"
    command "mkdir -p #{pub}"
end

execute 'move file' do
    user "root"
    command "mv wordpress/* #{pub}" 
end

execute 'delete file' do
    user "root"
    command "rm /tmp/wordpress.tar.gz"
end