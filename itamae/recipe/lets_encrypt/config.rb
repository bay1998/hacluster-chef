lets_encrypt = node[:lets_encrypt]

if lets_encrypt["ssl"]
    execute "generate ssl cert" do
        user "root"
        command "certbot certonly --key-type ecdsa --agree-tos --webroot -w #{lets_encrypt[:public]} -d #{lets_encrypt[:domain]} --register-unsafely-without-email -n"
    end

    execute "permit file" do
        user "root"
        command "chown -R nginx:nginx /etc/letsencrypt/{archive,live}/#{lets_encrypt[:domain]} && chmod -R 440 /etc/letsencrypt/{archive,live}/#{lets_encrypt[:domain]}"
    end

    service "nginx" do
        action [:restart]
    end
end