require 'helper/spec_helper'

%w{certbot}.each do |pkg|
    describe package(pkg) do
      it { should be_installed }
    end
end

lets_encrypt = node[:lets_encrypt]
certdir = "/etc/letsencrypt/archive/#{lets_encrypt[:domain]}"

%W(
    #{certdir}/fullchain1.pem
    #{certdir}/chain1.pem
    #{certdir}/privkey1.pem
    #{certdir}/cert1.pem
).each do |f|
    describe file(f) do
        it { should be_owned_by 'nginx' }
        it { should be_readable.by('owner') }
        it { should be_mode '440' }
    end
    describe file(f.gsub("1.",".").gsub("/archive/","/live/")) do
        it { should be_owned_by 'nginx' }
        it { should be_readable.by('owner') }
        it { should be_symlink }
        it { should be_linked_to f.gsub("/etc/letsencrypt", "../..") }
    end
end

describe port(443) do
    it { should be_listening }
end

describe service('nginx') do
    it { should be_running } 
end