require 'helper/spec_helper'

php = node[:php]
version = php[:version]
packages = php[:packages]
conffile = "/etc/php-fpm.d/www.conf"
version_short = version.split("-")[0]

packages.each do |p|
    describe package(p) do
        it { should be_installed.by('rpm').with_version(version_short) }  if p.tr("-","")
    end
end

describe service('php-fpm') do
    it { should be_enabled }
    it { should be_running } 
end

describe file(conffile) do
    it { should contain "user = nginx" }
    it { should contain "group = nginx" }
    it { should contain "listen = /run/php-fpm/www.sock" }
end